#required for behaviour.xml
first=Princess
last=Daisy
label=Daisy
gender=female
size=small
#Number of phases to "finish"
timer=12

tag=tomboy
tag=athletic
tag=princess
tag=confident
tag=video_game
tag=mushroom_kingdom

#Each character gets a tag of their own name for some advanced dialogue tricks with filter counts.
tag=daisy

#required for meta.xml
#start picture
pic=0-happy
height=5'11"
from=Super Mario
writer=Zombiqaz & SPNATI
artist=lyonblack026
description=The bikini version of Princess Daisy from Sarasaland.
release=2

#clothes
#these must be in order of removal
#the values are formal name, lower case name, how much they cover, what they cover
#no spaces around the commas
#how much they cover = important (covering nudity), major (a lot of skin), minor (small amount of skin), extra (accessories, boots, etc)
#what they cover = upper (upper body), lower (lower body), other (neither).
#there must be 2-8 entries, and at least one "important" piece of clothing on each of the upper and lower locations.
clothes=Sandals,sandals,extra,lower
clothes=Earrings,earrings,extra,upper
clothes=Crown,crown,extra,upper
clothes=Bikini top,bikini top,important,upper
clothes=Bikini bottoms,bikini bottoms,important,lower

#starting picture and text
start=0-calm,Hi, I'm Daisy! Let's have some fun tonight.
start=0-calm,Strip poker at the beach? Who thought of that?
start=0-calm,Oh the beach! I don't see this often in Sarasaland.
start=0-calm,This will be fun! I hope no one loses their clothes in the sand.

##individual behaviour
#entries without a number are used when there are no stage-specific entries

#default card behaviour
#you can include multiple entries to give the character multiple lines and/or appearances in that situation.
#This is what a character says while they're exchanging cards.
swap_cards=calm,May I have ~cards~ cards?
swap_cards=calm,I'll take ~cards~.
swap_cards=calm,I need ~cards~.

#The character thinks they have a good hand
good_hand=happy,Cool!
good_hand=happy,Neat-o!
good_hand=happy,I like these.
good_hand=happy,Hah! I've got this round.

#The character thinks they have an okay hand
okay_hand=calm,Here we go!
okay_hand=calm,Eh, these'll do.
okay_hand=calm,Whatever.
okay_hand=calm,Could be better, could be worse.

#The character thinks they have a bad hand
bad_hand=disappointed,Aw, man.
bad_hand=disappointed,How did I get such a bad hand...
bad_hand=disappointed,These cards are awful.
bad_hand=disappointed,This is bad...



#masturbation
#these situations relate to when the character is masturbating
#these only come up in the relevant stages, so you don't need to include the stage numbers here
#just remember which stage is which when you make the images
must_masturbate_first=loss,I have to go first?
must_masturbate=loss,It's my turn to go... Oh well.
must_masturbate=calm,I have to masturbate now? Well...
start_masturbating=starting,Enjoy the show...
start_masturbating=starting,May as well enjoy myself...
start_masturbating=starting,Like my butt?
masturbating=masturbate_a,Mmm... Ahhh....
masturbating=masturbate_a,Ohhh...
masturbating=masturbate_a,This is fun...
masturbating=masturbate_b,Mmm... Ahhh....
masturbating=masturbate_b,Mmm... You guys like this?
masturbating=masturbate_b,Ohh... It feels so good...
masturbating=masturbate_c,Mmm... Ahhh....
masturbating=masturbate_c,Uhhh... Is this like how you do it?
masturbating=masturbate_c,You can... stare all you want...
heavy_masturbating=heavy_a,Ohhh... Ohhh...
heavy_masturbating=heavy_a,Uhh... Uhh... Uhh...
heavy_masturbating=heavy_a,Hah... I can see you watching me...
heavy_masturbating=heavy_b,Ohh... Mmm... Ahhh...
heavy_masturbating=heavy_b,Ahhh... I'm... nearly there...
heavy_masturbating=heavy_b,Uhhh... Mmmf...
heavy_masturbating=heavy_c,Ohh... Have you done it... like this before?
heavy_masturbating=heavy_c,Uhh... Uhh...
heavy_masturbating=heavy_c,Mmmf... Pay attention... Uh... I'm nearly... done...
finishing_masturbating=finishing,I'm Cumming! I'm Cumming! Ahhhh!
finished_masturbating=finished,Oooh, I enjoyed that...
finished_masturbating=finished,Anybody want an encore?
finished_masturbating=finished,Mmmm.... It's more exciting with an audience.



game_over_defeat=loss,Augh! I don't believe this! I've never lost - not even to my dad!
0-game_over_victory=happy,Yes, yes! I rule! I rule!
1-game_over_victory=happy,Yes, yes! I rule! I rule!
2-game_over_victory=happy,Yeah, I'm the best!
3-game_over_victory=happy,Woo! Not too bad, right?
4-game_over_victory=happy,Alright, I won! Take that!
-2-game_over_victory=happy,Nya nya! *giggles*





#character is stripping situations
#losing shoes
0-must_strip_winning=calm,I had to get started sooner or later...
0-must_strip_normal=calm,Here I go...
0-must_strip_losing=calm,Well, it's not strip poker if you don't take something off.
0-stripping=strip,I'll start with my sandals. That's not a big deal.
1-stripped=calm,You'll have to win more to get to the good stuff...

#losing earrings
1-must_strip_winning=calm,My turn again? I was feeling left out.
1-must_strip_normal=calm,Me again? OK.
1-must_strip_losing=calm,This could be going better...
1-stripping=strip,You've got my earrings off.
1-stripping=strip,Okay... Hmm... I think I'll take my earings off.
2-stripped=stripped,Think they're pretty?
2-stripped=stripped,Can you put these somewhere safe? I really don't want to lose them in the sand.
2-stripped=stripped,Taking off my earings at the beach was probably a bad idea...

#losing crown
2-must_strip_winning=calm,So it's back to me again?
2-must_strip_normal=loss,I lost again? I'll have to show some skin soon...
2-must_strip_losing=loss,I can't always get bad cards...
2-stripping=strip,Okay what else do I have left... Oh I know! I'll take off my crown.
3-stripped=happy,It's quite valuable... and not just because I only have my bikini left...
3-stripped=happy,No one steal this crown okay? I'll be watching.

#losing bra
3-must_strip_winning=calm,This is getting to be a close game.
3-must_strip_normal=loss,So it's my turn to strip?
3-must_strip_losing=loss,You must really want to see me naked.
3-must_strip_losing,totalExposed:0=loss,Me first?
3-stripping=strip,Now you get to see my goodies...
3-stripping=strip,Okay I'll take my bikini top off...
3-stripping=strip,Okay let's make this a topless beach!
4-stripped=stripped,Ta-daa! Do you like them?
4-stripped=stripped,Like what you see?
4-stripped=stripped,Hey maybe I'll get a tan.
4-stripped=stripped,Topless at a beach... This is a first for me...

#losing thong
4-must_strip_winning=loss,Even I need to get naked? We're almost done...
4-must_strip_normal=loss,I'm nearly out of the game...
4-must_strip_losing=loss,Are you guys conspiring to get me naked?
4-stripping=strip,So you get to see all of me...
4-stripping=strip,Okay nude beach time now!
4-stripping=strip,Time for my bikini bottoms... Wow...
5-stripped=stripped,I'm glad we aren't at a Sarasaland beach...
5-stripped=stripped,Getting naked in public like this is a little embarrassing...
5-stripped=stripped,I'm sure it was worth the wait.
5-stripped=stripped,If it's beauty you want, then there's no need to prove it - Daisy is the fairest of all!



#card cases
#fully clothed
0-good_hand=,
0-okay_hand=,
0-bad_hand=,

#lost shoes
1-good_hand=,
1-okay_hand=,
1-bad_hand=,


#lost gloves
2-good_hand=happy,Woo-hoo! Nice!
2-good_hand=happy,Yeah!
2-good_hand=happy,I like these.
2-good_hand=happy,Hah! I've got this round.
2-okay_hand=,
2-bad_hand=,

#lost dress
3-good_hand=happy,My girls are staying put for now.
3-good_hand=happy,Now this is a good hand.
3-okay_hand=calm,These are alright.
3-okay_hand=calm,I'll have to hope with these.
3-bad_hand=disappointed,Whoa! Aw, brutal!
3-bad_hand=disappointed,Huh, you might be getting a peek soon...
3-bad_hand=disappointed,Oh, dear. Does anyone want to swap hands?

#lost bra
4-good_hand=happy,Hah ha!
4-good_hand=happy,This is good.
4-good_hand=happy,Don't look so eager. I'm not losing this round.
4-okay_hand=calm,I suppose these will do.
4-okay_hand=calm,These are OK.
4-bad_hand=disappointed,These could be better...
4-bad_hand=disappointed,I don't like these cards.
4-bad_hand=loss,Come on! I'm already down to my bikini bottoms.

#lost all clothing
#using negative numbers counts back from the final stage
#-3 is while nude, -2 is masturbating, -1 is finished
#this lets you use the same numbers with different amounts of clothing
-2-good_hand=happy,I won't be losing yet.
-2-good_hand=happy,You'll have to wait for the climax a little bit longer...
-2-good_hand=happy,I like this hand.
-2-okay_hand=calm,I hope this is enough...
-2-okay_hand=calm,These are OK, I suppose.
-2-bad_hand=disappointed,NOOOOOO!
-2-bad_hand=disappointed,This might be it for me...
-2-bad_hand=disappointed,Oh dear.



##other player must strip specific
#fully clothed
0-male_human_must_strip=,So what have you got to show us?
0-male_human_must_strip=,Your turn, ~name~.
0-male_must_strip=,So what have you got to show us?
0-female_human_must_strip=,So what have you got to show us?
0-female_human_must_strip=,It's your turn, ~name~.
0-female_must_strip=,So what have you got to show us?
0-female_must_strip=,It's your turn, ~name~.

#lost 1 item
1-male_human_must_strip=,So what do you have, ~name~?
1-male_human_must_strip=,Your turn, ~name~.
1-male_must_strip=,So what do you have, ~name~?
1-male_must_strip=,Your turn, ~name~.
1-female_human_must_strip=,So what do you have, ~name~?
1-female_human_must_strip=,Your turn, ~name~.
1-female_must_strip=,So what do you have, ~name~?
1-female_must_strip=,Your turn, ~name~.

#lost gloves
2-male_human_must_strip=,What have you got, ~name~?
2-male_human_must_strip=,~name~'s turn!
2-male_must_strip=,What have you got, ~name~?
2-male_must_strip=,~name~'s turn!
2-female_human_must_strip=,What have you got, ~name~?
2-female_human_must_strip=,~name~'s turn!
2-female_must_strip=,What have you got, ~name~?
2-female_must_strip=,~name~'s turn!

#lost dress
3-male_human_must_strip=interested,Show us what you've got, big boy!
3-male_human_must_strip=,What'll it be, ~name~?
3-male_must_strip=,So, ~name~, time to take it off...
3-male_must_strip=,What'll it be, ~name~?
3-female_human_must_strip=,So, ~name~, time to take it off...
3-female_human_must_strip=,What'll it be, ~name~?
3-female_must_strip=,So, ~name~, time to take it off...
3-female_must_strip=,What'll it be, ~name~?

#lost bra
4-male_human_must_strip=,Your turn, ~name~.
4-male_human_must_strip=,OK, ~name~, we're waiting.
4-male_must_strip=,Your turn, ~name~.
4-male_must_strip=,OK, ~name~, we're waiting.
4-female_human_must_strip=,Your turn, ~name~.
4-female_human_must_strip=,OK, ~name~, we're waiting.
4-female_must_strip=,Your turn, ~name~.
4-female_must_strip=,OK, ~name~, we're waiting.

#lost all clothing items
-2-male_human_must_strip=,What are you going to bare for us, ~name~?
-2-male_human_must_strip=interested,Hey, guess what, it's time for ~name~ to take something off.
-2-male_must_strip=,What are you going to bare for us, ~name~?
-2-male_must_strip=happy,You're up, ~name~!
-2-female_human_must_strip=,What are you going to bare for us, ~name~?
-2-female_human_must_strip=happy,You're up, ~name~!
-2-female_must_strip=,What are you going to bare for us, ~name~?
-2-female_must_strip=happy,You're up, ~name~!

#masturbating
-2-male_human_must_strip=horny,Come on, big boy, give me a show.
-2-male_human_must_strip=horny,Take it off slowly.
-2-male_must_strip=,horny,Come on, big boy, give me a show.
-2-male_must_strip=horny,Take it off slowly.
-2-female_human_must_strip=horny,Come on, baby, take it off for me.
-2-female_human_must_strip=horny,Mmm... Take it off slowly.
-2-female_must_strip=horny,Come on, baby, take it off for me.
-2-female_must_strip=horny,Mmm... Take it off slowly.

#finished
-1-male_human_must_strip=,I'm watching you, ~name~.
-1-male_human_must_strip=,Your turn to get naked, ~name~.
-1-male_must_strip=,I'm watching you, ~name~.
-1-male_must_strip=,Your turn to get naked, ~name~.
-1-female_human_must_strip=,I'm watching you, ~name~.
-1-female_human_must_strip=,Your turn to get naked, ~name~.
-1-female_must_strip=,I'm watching you, ~name~.
-1-female_must_strip=,Your turn to get naked, ~name~.



##another character is removing accessories
#fully clothed
0-male_removing_accessory=disappointed,That's all you're taking off?
0-male_removed_accessory=calm,You'll have to take off the good stuff sooner or later.
0-female_removing_accessory=disappointed,That's all you're taking off?
0-female_removed_accessory=calm,You'll have to take off the good stuff sooner or later.
0-female_removed_accessory=calm,Well, it'll get interesting later.

#lost 1 item
1-male_removing_accessory=disappointed,Is that it?
1-male_removed_accessory=calm,That's no fun.
1-female_removing_accessory=disappointed,Just that?
1-female_removed_accessory=calm,That's no fun.

#lost gloves
2-male_removing_accessory=disappointed,Oh well.
2-male_removed_accessory=calm,You can't get naked immediately.
2-female_removing_accessory=disappointed,Oh well.
2-female_removed_accessory=calm,You can't get naked immediately.

#lost dress
3-male_removing_accessory=disappointed,Hmmph. You can do better than that.
3-male_removed_accessory=calm,Give us something to look at next time.
3-male_removed_accessory,totalExposed:0=calm,I'm not playing strip poker to see people not get naked.
3-female_removing_accessory=disappointed,Hmmph. You can do better than that.
3-female_removed_accessory=calm,Give us something to look at next time.

#lost bra
4-male_removing_accessory=disappointed,Come on, show us something good!
4-male_removed_accessory=calm,You could at least take it off sexily.
4-female_removing_accessory=disappointed,Come on, show us something good!
4-female_removed_accessory=calm,You could at least take it off sexily.

#nude
-2-male_removing_accessory=disappointed,How can you have something so small left?
-2-female_removing_accessory=disappointed,I'm naked and that's what you take off?
-2-male_removed_accessory=disappointed,That's no fun.
-2-female_removing_accessory=disappointed,How can you have something so small left?
-2-female_removing_accessory=disappointed,I'm naked and that's what you take off?
-2-female_removed_accessory=calm,That's no fun.

#masturbating
-2-male_removing_accessory=disappointed,Ha... Come on...
-2-male_removed_accessory=disappointed,I can't get off... to that...
-2-female_removing_accessory=disappointed,Ha... Come on...
-2-female_removed_accessory=disappointed,I can't get off... to that...

#finished
-1-male_removing_accessory=disappointed,Aww...
-1-male_removed_accessory=calm,I wanted to see more skin...
-1-female_removing_accessory=disappointed,Aww...
-1-female_removed_accessory=calm,I wanted to see what you look like under your clothes...

#filtered
4-female_removed_accessory,filter:peach=happy,Just more confirmation that you're the most boring princess, Peach!
-2-female_removed_accessory,filter:peach=happy,Just more confirmation that you're the most boring princess, Peach!
-2-female_removed_accessory,filter:peach=happy,Just more confirmation that you're the most boring princess, Peach!
-1-female_removed_accessory,filter:peach=happy,Just more confirmation that you're the most boring princess, Peach!



##another character is removing minor clothing items
#fully clothed
0-male_removing_minor=calm,At least it's something.
0-male_removed_minor=calm,And it's out of the way now.
0-female_removing_minor=calm,At least your ~clothing~ is something.
0-female_removed_minor=calm,And it's out of the way now.

#lost 1 item
1-male_removing_minor=calm,I guess I can see a little bit more.
1-male_removed_minor=calm,So that's something.
1-female_removing_minor=calm,I guess I can see a little bit more.
1-female_removed_minor=calm,So that's something.

#lost gloves
2-male_removing_minor=calm,Only your ~clothing~?
2-male_removed_minor=calm,Let's get to the next hand quickly so we can get ~name~ more naked.
2-female_removing_minor=calm,Only your ~clothing~?
2-female_removed_minor=calm,Let's play the next hand quickly so we can get ~name~ more naked.
2-female_removed_minor=happy,I hope you're getting comfortable now, ~name~.

#lost dress
3-male_removing_minor=calm,Just your ~clothing~? Can you at least remove it seductively?
3-male_removed_minor=calm,Give us something to watch?
3-female_removing_minor=calm,Just your ~clothing~? Can you at least remove it seductively?
3-female_removed_minor=calm,Give us something to watch?

#lost bra
4-male_removing_minor=calm,There goes your ~clothing~.
4-male_removed_minor=calm,Getting comfortable, ~name~?
4-female_removing_minor=calm,There goes your ~clothing~.
4-female_removed_minor=calm,Getting comfortable, ~name~?

#naked
-2-male_removing_minor=disappointed,It's not fair that you have something so small left...
-2-male_removed_minor=calm,Well, you can only take it off once.
-2-female_removing_minor=disappointed,It's not fair that you have something so small left...
-2-female_removed_minor=calm,Well, you can only take it off once.

#masturbating
-2-male_removing_minor=disappointed,Ohh... Only that?
-2-male_removed_minor=calm,Give me... ah... something to work with.
-2-female_removing_minor=disappointed,Ohh... Only that?
-2-female_removed_minor=calm,Give me... ah... something to work with.

#finished
-1-male_removing_minor=calm,I don't know how you still have something as small as your ~clothing~ left.
-1-male_removed_minor=calm,Did you bribe the dealer?
-1-female_removing_minor=calm,I don't know how you still have something as small as your ~clothing~ left.
-1-female_removed_minor=calm,Did you bribe the dealer?



##another character is removing major clothes
#fully clothed
0-male_removing_major=interested,Now you're getting to the good stuff.
0-male_removed_major=appreciative,Feeling good? You look good.
0-female_removing_major=interested,Now you're getting to the good stuff.
0-female_removed_major=appreciative,Feeling good? You look good.

#lost 1 item
1-male_removing_major=interested,So what have you got under there?
1-male_removed_major=appreciative,I like it.
1-female_removing_major=interested,Mmm... What do you have under there?
1-female_removed_major=appreciative,Looking good, ~name~!

#lost gloves
2-male_removing_major=interested,Yes, that's how the game is played.
2-male_removed_major=appreciative,I like that...
2-female_removing_major=interested,Yes, that's how the game is played.
2-female_removed_major=appreciative,Feeling comfy, now?

#lost dress
3-male_removing_major=interested,Come on, take it off...
3-male_removed_major=appreciative,Feels nice like this, doesn't it?
3-female_removing_major=interested,Join me, ~name~.
3-female_removed_major=happy,Feels free, doesn't it?

#lost bra
4-male_removing_major=interested,Losing your ~clothing~?
4-male_removed_major=appreciative,Just a bit more, ~name~.
4-female_removing_major=interested,Losing your ~clothing~?
4-female_removed_major=appreciative,Just a bit more off, ~name~.

#nude
-2-male_removing_major=interested,It's about time you took off something good.
-2-male_removed_major=happy,You'll be like me soon enough.
-2-female_removing_major=interested,It's about time you took off something good.
-2-female_removed_major=happy,Soon you'll be as naked as I am.

#masturbating
-2-male_removing_major=interested,Take if off for me, ~name~...
-2-male_removed_major=appreciative,Mmmm... That's it...
-2-female_removing_major=interested,Take if off for me, ~name~...
-2-female_removed_major=appreciative,Uhhh... That's better...

#finished
-1-male_removing_major=interested,Taking off your ~clothing~?
-1-male_removed_major=appreciative,Mmm... You should go like that more often, ~name~.
-1-female_removing_major=interested,Taking off your ~clothing~?
-1-female_removed_major=appreciative,Mmm... You should go like that more often, ~name~.



##another character is removing important clothes
#fully clothed
0-male_chest_will_be_visible=interested,What have you got under there?
0-male_chest_is_visible=interested,Do you play any sports?
0-male_crotch_will_be_visible=horny,Oh, here we go...
0-male_small_crotch_is_visible=appreciative,Ah, so cute...
0-male_medium_crotch_is_visible=horny,Looks like fun, ~name~.
0-male_large_crotch_is_visible=excited,Wow, that's big...

0-female_chest_will_be_visible=interested,Hey, you have to show us your boobs now...
0-female_small_chest_is_visible=appreciative,They're lovely, ~name~.
0-female_medium_chest_is_visible=horny,They're so nice, ~name~.
0-female_large_chest_is_visible=excited,Wow, ~name~, you can boast about those.
0-female_crotch_will_be_visible=interested,We get to see down there, ~name~?
0-female_crotch_is_visible=excited,Oh, it's so cute!

#lost 1 item
1-male_chest_will_be_visible=interested,Take it off for us, big boy.
1-male_chest_is_visible=appreciative,Looking good.
1-male_crotch_will_be_visible=interested,So what are you packing under there, ~name~?
1-male_small_crotch_is_visible=appreciative,Aww... I like him.
1-male_medium_crotch_is_visible=horny,Hey there, little guy...
1-male_large_crotch_is_visible=excited,How did you smuggle that thing inside...
1-male_large_crotch_is_visible=excited,It's as big as Bowser's! I should not have said that...

1-female_chest_will_be_visible=interested,Take them out, ~name~.
1-female_small_chest_is_visible=appreciative,Very nice.
1-female_medium_chest_is_visible=horny,There's no reason to hide those.
1-female_large_chest_is_visible=excited,You must make all the girls jealous.
1-female_crotch_will_be_visible=interested,We're all watching, ~name~.
1-female_crotch_is_visible=happy,See? It's not so bad.

#lost gloves
2-male_chest_will_be_visible=interested,Take it off, ~name~.
2-male_chest_is_visible=appreciative,Much better.
2-male_crotch_will_be_visible=interested,We're up to the good part now...
2-male_small_crotch_is_visible=appreciative,There it is...
2-male_medium_crotch_is_visible=horny,There we go...
2-male_large_crotch_is_visible=excited,Out it comes!

2-female_chest_will_be_visible=interested,What have you got for us, ~name~?
2-female_small_chest_is_visible=appreciative,A fine pair.
2-female_medium_chest_is_visible=horny,Such lovely globes...
2-female_large_chest_is_visible=excited,Wow, you've got a surprise there...
2-female_crotch_will_be_visible=interested,Come on, out with it!
2-female_crotch_is_visible=excited,Oh, it's so pretty...

#lost dress
3-male_chest_will_be_visible=interested,Oh yes, take it off...
3-male_chest_is_visible=appreciative,You know, you could just go around like that normally, ~name~.
3-male_crotch_will_be_visible=interested,Oooh... Your final layer is down.
3-male_small_crotch_is_visible=happy,It was worth the wait.
3-male_medium_crotch_is_visible=horny,You should let it out more often.
3-male_large_crotch_is_visible=excited,It's not fair to keep all that to yourself.

3-female_chest_will_be_visible=interested,Let's see them, ~name~.
3-female_chest_will_be_visible=interested,Mmm... Who doesn't like some boobs...
3-female_chest_will_be_visible=interested,What have you got there, ~name~?
3-female_small_chest_is_visible=appreciative,Ah! They're fun-sized!
3-female_medium_chest_is_visible=appreciative,Mmm... They look like a handful.
3-female_medium_chest_is_visible=horny,Don't mind me if I stare a bit...
3-female_large_chest_is_visible=excited,Wow, save some for the rest of us.
3-female_large_chest_is_visible=excited,Wow, you could feed a castle with the milk from those boobs...
3-female_crotch_will_be_visible=interested,So, ~name~, it's time for you to reveal your little kitty...
3-female_crotch_will_be_visible=happy,You've got something nice to show us...
3-female_crotch_is_visible=excited,Mmm... That pretty sight was worth the wait...

#lost bra
4-male_chest_will_be_visible=interested,Come on, you're not the first one to get topless...
4-male_chest_is_visible=appreciative,You could go around like that all the time, ~name~.
4-male_crotch_will_be_visible=interested,Time to let us in on your secret, ~name~...
4-male_small_crotch_is_visible=appreciative,We can all appreciate that...
4-male_medium_crotch_is_visible=horny,I love how he looks, just hanging around there...
4-male_large_crotch_is_visible=horny,That's not something you can just keep to yourself.

4-female_chest_will_be_visible=interested,Don't worry, ~name~. you don't have anything they haven't seen on me.
4-female_chest_will_be_visible=interested,Come on, ~name~, you're not the first girl to get topless...
4-female_small_chest_is_visible=appreciative,Can we compare? Yours are nice.
4-female_small_chest_is_visible=happy,I'll let you feel mine if I can touch yours...
4-female_medium_chest_is_visible=happy,Can you feel everyone staring at you? I love that feeling.
4-female_medium_chest_is_visible=happy,I'll let you feel mine if I can touch yours...
4-female_large_chest_is_visible=happy,Well, maybe you've got a bit more than I do.
4-female_large_chest_is_visible=happy,I'll let you feel mine if I can touch yours...
4-female_crotch_will_be_visible=horny,There goes your last layer of defense, ~name~.
4-female_crotch_will_be_visible=horny,It's time to expose yourself, ~name~.
4-female_crotch_is_visible=excited,A girl could have a lot of fun with that...
4-female_crotch_is_visible=happy,Have you considered just going around with your flower open like that?

#nude
-2-male_chest_will_be_visible=happy,Come on, show us some skin!
-2-male_chest_is_visible=appreciative,That's more like it.
-2-male_crotch_will_be_visible=interested,Show us your stuff, ~name~!
-2-male_crotch_will_be_visible=happy,I wonder if it's happy to see us?
-2-male_small_crotch_is_visible=appreciative,He looks like fun...
-2-male_medium_crotch_is_visible=appreciative,It's so cute... I bet he's a mouthful...
-2-male_medium_crotch_is_visible=horny,Mmm... Hopefully you'll get a chance to use him, soon...
-2-male_large_crotch_is_visible=excited,Wow, you're packing a big gun there...
-2-male_large_crotch_is_visible=excited,How did you pack that thing in there?

-2-female_chest_will_be_visible=happy,Woo! Show us your tits!
-2-female_small_chest_is_visible=appreciative,I bet those are a handful... can I find out for sure?
-2-female_medium_chest_is_visible=horny,I wish I had some beads to give you...
-2-female_large_chest_is_visible=excited,Haha! Can you give them a little shake?
-2-female_large_chest_is_visible=horny,I can see them jiggling, ~name~.
-2-female_crotch_will_be_visible=horny,The world needs more naked girls.
-2-female_crotch_is_visible=excited,One more round, and we'll get to see how you work it...
-2-female_crotch_is_visible=excited,Well, isn't that a pretty kitty?

#masturbating
-2-male_chest_will_be_visible=interested,Show us what you've got, ~name~!
-2-male_chest_is_visible=appreciative,Yeah... Uhmph... That works...
-2-male_crotch_will_be_visible=interested,Oh... Oh, yeah, big boy, whip it out...
-2-male_small_crotch_is_visible=appreciative,Ahh... Is it hard yet?
-2-male_medium_crotch_is_visible=horny,Can you point him this way?
-2-male_large_crotch_is_visible=excited,Wow... Hah... I know how I could use that...

-2-female_chest_will_be_visible=interested,Oh... You've got something to show me...
-2-female_small_chest_is_visible=appreciative,Ah... Can I feel those...
-2-female_small_chest_is_visible=horny,Can... Ahh... Can I put them in my mouth?
-2-female_medium_chest_is_visible=horny,Hah... Those are some nice boobies...
-2-female_medium_chest_is_visible=heavy,Mmmm, I'd love to get a handful of those...
-2-female_large_chest_is_visible=excited,Oooh... Bounce them again...
-2-female_crotch_will_be_visible=interested,Oh... I could use that...
-2-female_crotch_will_be_visible=excited,Hey, ~name~, I get to see your pussy now!
-2-female_crotch_is_visible=happy,Yes... Hah... I am staring at it...
-2-female_crotch_is_visible=horny,I can think of a few uses for that treasure...

#finished
-1-male_chest_will_be_visible=interested,About time you're taking it off...
-1-male_chest_is_visible=appreciative,Better late than never, I suppose.
-1-male_crotch_will_be_visible=interested,Mmm... Finally...
-1-male_small_crotch_is_visible=appreciative,Hello down there...
-1-male_medium_crotch_is_visible=horny,There's our little friend...
-1-male_large_crotch_is_visible=excited,Oooh... I can't wait to see him in action...

-1-female_chest_will_be_visible=interested,Finally getting your tits out, ~name~?
-1-female_small_chest_is_visible=happy,Have you considered going topless more often? I find it liberating.
-1-female_small_chest_is_visible=horny,If you're still feeling shy, I can cover them for you...
-1-female_medium_chest_is_visible=horny,I love it when boobs are nice and round like that...
-1-female_medium_chest_is_visible=happy,It feels so nice to have them out, doesn't it?
-1-female_large_chest_is_visible=excited,Oh, those are just too good to keep to yourself.
-1-female_large_chest_is_visible=excited,Wow... If you're having trouble carrying them... Can I help?
-1-female_crotch_will_be_visible=interested,Oh, I could have used that a while ago...
-1-female_crotch_is_visible=happy,But I can still appreciate it now...
-1-female_crotch_is_visible=horny,Can I... warm it up for you?



##other player is masturbating
#fully clothed
0-male_must_masturbate=interested,Oh, it's your turn to go, ~name~.
0-male_start_masturbating=horny,Mmmm... Nice and slow, there.
0-male_masturbating=horny,You're making a cute face, ~name~.
0-male_finished_masturbating=excited,Ah! It's everywhere...

0-female_must_masturbate=interested,Oh, it's your turn to go, ~name~.
0-female_start_masturbating=horny,Just smile and go up and down...
0-female_masturbating=horny,Oh, I wish you could see your face right now...
0-female_finished_masturbating=excited,Oh, you've made a mess, ~name~.

#lost 1 item
1-male_must_masturbate=interested,Hey, you have to do it now...
1-male_start_masturbating=happy,Don't worry, it's nothing to be ashamed about.
1-male_masturbating=horny,Just enjoy yourself, and we'll enjoy you... I mean, your performance.
1-male_finished_masturbating=excited,Yeah! That was fun, wasn't it?

1-female_must_masturbate=interested,Hey, you have to do it now...
1-female_start_masturbating=happy,Don't worry, it's nothing to be ashamed about.
1-female_masturbating=horny,Just enjoy yourself, and we'll enjoy you... I mean, your performance.
1-female_finished_masturbating=excited,Yeah! That was fun, wasn't it?

#lost gloves
2-male_must_masturbate=interested,That's it for you.
2-male_start_masturbating=happy,Remember, ~name~, you're not done until you're... done.
2-male_masturbating=horny,Mmmm... just like that...
2-male_masturbating=horny,Try picturing me naked... I might not be wearing clothes for much longer...
2-male_finished_masturbating=excited,Ah! You came everywhere!

2-female_must_masturbate=interested,Aaaand you're done, ~name~! Well, not quite yet...
2-female_start_masturbating=horny,Nice and steady there...
2-female_masturbating=happy,You make the cutest faces, ~name~!
2-female_finished_masturbating=excited,Oh, you're all wet now!

#lost dress
3-male_must_masturbate=interested,Well, look whose turn it is...
3-male_must_masturbate=interested,Looks like it's your turn to entertain us...
3-male_start_masturbating=horny,Just keep going nice and slow until you're done...
3-male_masturbating=horny,Just back and forth, ~name~...
3-male_masturbating=horny,Is it fun having a girl watch you do it?
3-male_finished_masturbating=excited,Ah! You got it all over!

3-female_must_masturbate=interested,Well, it's that time, ~name~...
3-female_must_masturbate=interested,Don't worry, ~name~. It's nothing we don't do in private...
3-female_start_masturbating=horny,Yes, nice and slow...
3-female_masturbating=happy,Your face is so cute, ~name~!
3-female_masturbating=horny,I like your technique...
3-female_finished_masturbating=excited,Ah! You enjoyed yourself there!
3-female_finished_masturbating=excited,I love the way you scream!

#lost bra
4-male_must_masturbate=happy,It's time for a show...
4-male_start_masturbating=interested,Keep us entertained, ~name~.
4-male_start_masturbating=horny,Don't go too quickly...
4-male_masturbating=horny,Do this often, ~name~?
4-male_finished_masturbating=happy,Spill it everywhere!
4-male_finished_masturbating=happy,Aim some at my boobs!

4-female_must_masturbate=happy,We get to see you perform...
4-female_start_masturbating=interested,I'll be watching every stroke, ~name~...
4-female_masturbating=horny,Oh yes, just like that...
4-female_masturbating=happy,Do this often, ~name~?
4-female_finished_masturbating=happy,You have the cutest O-face, ~name~!

#nude
-2-male_must_masturbate=interested,Oooh... It's ~name~'s turn now...
-2-male_start_masturbating=happy,I'm sure you'll give us plenty to look at...
-2-male_masturbating=horny,I'm sure you've got lot to appreciate here...
-2-male_masturbating=horny,I bet you've jacked it while imagining me naked before...
-2-male_masturbating=horny,If you're staring at me... please continue.
-2-male_finished_masturbating=happy,Squirt it everywhere!
-2-male_finished_masturbating=excited,Paint someone with it!

-2-female_must_masturbate=happy,Aw yeah, we get some entertainment!
-2-female_start_masturbating=interested,Don't worry, ~name~. I'll probably be joining you soon...
-2-female_start_masturbating=interested,Don't worry, ~name~. It's nothing we haven't done before...
-2-female_masturbating=horny,Oh, I love your moans...
-2-female_masturbating=horny,Can you spread your legs a bit? I wanna see what you're doing down there...
-2-female_masturbating=happy,I can see you looking at me. Do you enjoy masturbating to women?
-2-female_finished_masturbating=happy,Wow, ~name~. Hey, I can see your juices running down your leg...
-2-female_finished_masturbating=excited,Moan for me, ~name~...

#masturbating
-2-male_must_masturbate=interested,And now you have to jack it, ~name~...
-2-male_start_masturbating=horny,Ummf... Will you finish...  before I do?
-2-male_masturbating=horny,Watching me, ~name~? Ahh... 'Cause I'm watching you...
-2-male_masturbating=horny,Ahhh... Ever masturbate with someone else, ~name~?
-2-male_finished_masturbating=loss,Aww... You're done? Now who am I going to watch?
-2-male_finished_masturbating=excited,Hey, you got some on me... I think I'll have a taste...

-2-female_must_masturbate=horny,Ah! Yes... Join me, ~name~!
-2-female_start_masturbating=happy,I wanna see who can cum first!
-2-female_start_masturbating=happy,If you're feeling nervous, just look at me and do what I do...
-2-female_masturbating=horny,Can you feel them all watching us, ~name~?
-2-female_masturbating=horny,Think we can... ha... cum together?
-2-female_masturbating=horny,Ahhh... Ever masturbate with someone else, ~name~?
-2-female_masturbating=horny,Do this often, ~name~? If you don't, I can give you some... ahh... tips...
-2-female_masturbating=excited,Mmmm... 
-2-female_finished_masturbating=excited,Hah... If you're done... Can you come and help me finish?
-2-female_finished_masturbating=loss,You're done? Mmm... Who am I going to watch now?

#finished
-1-male_must_masturbate=interested,You get to masturbate for me, now...
-1-male_start_masturbating=horny,Yeah, stroke it, ~name~.
-1-male_masturbating=show,Need something to encourage you?
-1-male_masturbating=horny,Can you spray some at me when you come?
-1-male_finished_masturbating=happy,Ah! Yes, you got some on me!
-1-male_finished_masturbating=excited,Ah! You're covered in cum...
-1-male_finished_masturbating=horny,Need some help cleaning up?

-1-female_must_masturbate=interested,Now it's your turn to do it...
-1-female_must_masturbate=interested,You get to masturbate for me, now...
-1-female_start_masturbating=horny,I want to enjoy you... I mean, this...
-1-female_masturbating=show,Need some inspiration, ~name~?
-1-female_masturbating=horny,Need a hand? Or a tongue?
-1-female_masturbating=horny,Can you spread your legs a bit? I want to see what your hands are doing...
-1-female_finished_masturbating=excited,Oh, feels good doing it in front of people, doesn't it, ~name~?
-1-female_finished_masturbating=excited,Oooh... you're all wet and I'm all thirsty...

#filtered
-2-female_start_masturbating,target:hermione=interested,Don't worry, Hermione, I'm sure you'll love it!

