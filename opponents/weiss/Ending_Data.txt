#Weiss's Ending:
    
    
    All of this is based on a scene in Volume 4, Chapter 6 in case you want reference.

ending=Rebellious Streak
        ending_gender=any

        screen=ending-1.png
#A ballroom looking area with a bunch of random rich-looking characters.  If it's easier, just make them all completely black.

                text=The Schnee Mansion, filled with the richest, most powerful people in the world.
                x=14%
                y=14%
                width=20%
				arrow=none
                
                   text=Though, really, you find them all terrible.  Just... terrible.  
                x=14%
                y=26%
                width=20%


   text=At least the food is good.  You can't pronounce any of it, but it's good.  
                x=14%
                y=26%
                width=20%
				arrow=none
                
                
 text=Wait, why were you here again?
                x=66%
                y=20%
                width=20%
				arrow=none

        screen=ending-2.png
#Volume 4 Weiss appears on screen, looking off to the side.  Isn't happy.  

                text=Oh, it's Weiss!  This IS her mansion after all, though you see she doesn't exactly like it here.  
                x=14%
                y=14%
                width=20%
				arrow=none

                text=She might have mentioned how she didn't get along with her father.  
                x=14%
                y=26%
                width=20%
				arrow=none

                text=It doesn't help that her father's a slave driver, so you see her point.
                x=66%
                y=20%
                width=20%
				arrow=none

        screen=ending-3.png
#Weiss looks at player, looks embarrassed and surprised.  

                text=Oh, right.  
                x=14%
                y=14%
                width=10%
				arrow=none

                text=You saw her naked...  And masturbating...
                x=14%
                y=18%
                width=20%
				arrow=none

                text=She probably isn't happy to see you.  Oh, well, back to fancy cakes.
                x=66%
                y=16%
                width=20%
				arrow=none
                
                  text=Weiss IS cute when she's embarrassed, though...
                x=66%
                y=26%
                width=20%
				arrow=none
                
                    screen=ending-4.png
#Some sort of balcony outside/out of the party.

text=You stepped outside for a moment. You can't take all the racism and assholishness.  
                x=14%
                y=14%
                width=20%
				arrow=none
                
                text=God, it's cold out...
                x=86%
                y=60%
                width=10%
				arrow=right

                text="Shut up!"  
                x=86%
                y=60%
				width=10%
                arrow=right
                
                 text="AHH!!!"
                x=88%
                y=66%
				width=8%
                arrow=right
                
                text=What the heck did you just miss!?  Was that a gunshot!?
                x=14%
                y=20%
				width=20%
                arrow=none
                
                


        screen=ending-5.png
#Weiss on the ground, angry, looking at her father.

                text=Whatever you missed, it was a big deal...  
                x=14%
                y=14%
				width=20%
                arrow=none

                text=Oh, Weiss is storming out.  Maybe you should follow her; everyone else looks afraid.  
                x=14%
                y=20%
				width=20%
                arrow=none

     screen=ending-6.png
#Outside/snowy garden area.  Weiss is angry/sad.

                text=Stupid!  Stupid stupid stupid!  
                x=10%
                y=32%
				width=10%
                arrow=right
                
                text=She's still fuming, so maybe you should stay back for a moment.  
                x=66%
                y=16%
				width=20%
                arrow=none

              text=They don't know anything about the real world!
                x=38%
                y=35%
				width=20%
                arrow=left

              text=I'm not a doll for my father to dress up and show off.  I'm allowed my own opinions!
                x=38%
                y=42%
				width=20%
                arrow=left

     screen=ending-7.png
#Outside/snowy garden area.  Weiss thinks about something.

                text=Hmm...  Dress up...  
                x=
                y=
                arrow= from Weiss

                text=I...  I AM alone out here.  
                x=
                y=
                arrow= from Weiss
                
                text=I could see his face...  His precious heiress doing something so... deplorable!  
                x=
                y=
                arrow= from Weiss
                
                 text=But my life is mine! 
                x=
                y=
                arrow= from Weiss
                
                 text=What is she talking about?
                x=
                y=
                arrow= 

     screen=ending-8.png
#Weiss removes her jacket.

                text=Oh.  This is interesting.  
                x=
                y=
                arrow= 

                text=I-I shouldn't be doing this...
                x=
                y=
                arrow= from Weiss

                text=No, please, continue.
                x=
                y=
                arrow= 

                text=B-but...
                x=
                y=
                arrow= from Weiss
                
     screen=ending-9.png
#Weiss starts to removes her dress.

                text=Jackpot!
                x=
                y=
                arrow= 

                text=I...  I need to make my own decisions!  Starting by getting rid of this stupid dress!  I want my combat skirt back!
                x=
                y=
                arrow= from Weiss
                
     screen=ending-10.png
#Weiss topless, in lacey panties.  Looking around to make sure no one's watching. 

     text=No bra?  
                x=
                y=
                arrow= 
                
     text=Nice. 
                x=
                y=
                arrow= 

                text=G-god, it's cold out here.  B-but I still feel warm inside...
                x=
                y=
                arrow= from Weiss
                
                      text=I'm doing something bad, father!  Ha!  I'm not yours to control!
                x=
                y=
                arrow= from Weiss
                
                
     text=Probably the only instance in which a girl's daddy issues could be a good thing.
                x=
                y=
                arrow= 
                
                
     screen=ending-11.png
#Weiss topless, in panties.  Looks happy, but nervous.  

     text=Ahh, this feels better than it should.
                x=
                y=
                arrow= from Weiss
                
     text=While I'm here...  
                x=
                y=
                arrow= from Weiss
                
                
     text=This can't become a thing for me, though.  People could find out!
                x=
                y=
                arrow= from Weiss
                
                
     text=Ha.
                x=
                y=
                arrow= 
                
     screen=ending-12
#Weiss starts removing her panties.

     text=Now time to remove these stupid panties!
                x=
                y=
                arrow= from Weiss
                
     text=Wow, I'm really glad Yang isn't around to hear that.
                x=
                y=
                arrow= from Weiss
                
            screen=ending-13.png
#Weiss stands naked, not covering herself.

            
  text=Well this is hot, even if everything around you is frozen.
                x=
                y=
                arrow= 

     text=Ah...  I still feel warm...
                x=
                y=
                arrow= from Weiss
                
     text=M-maybe I might do this more often...  
                x=
                y=
                arrow= from Weiss

  text=Awesome idea!  
                x=
                y=
                arrow= 

     text=Hmm?  
                x=
                y=
                arrow= from Weiss
                
                
     text=W-wait, you didn't say that out loud, did you?  
                x=
                y=
                arrow= from Weiss
                
                
            screen=ending-14.png
#Weiss stands naked, embarrassed and covering herself.

            
  text=AHH!!!
                x=
                y=
                arrow= from Weiss

     text=Well, she definitely isn't happy to see you now.  
                x=
                y=
                arrow= 
                
                
  text=W-why are you here!?
                x=
                y=
                arrow= from Weiss
                
                
  text=W-what are you doing in my garden!?
                x=
                y=
                arrow= from Weiss

  text=W-where!?  What!?  Why!?
                x=
                y=
                arrow= from Weiss
                
         screen=ending-15.png
#Weiss stands naked, angry and embarrassed, trying and failing to cover herself.

  text=Get out of here!!!
                x=
                y=
                arrow= from Weiss
                
                
  text=It might be a good time to leave.  
                x=
                y=
                arrow=
                
                
  text=Thanks for the party favor, Weiss!
                x=
                y=
                arrow=